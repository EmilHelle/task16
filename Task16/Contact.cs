﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task16
{
    class Contact
    {
        private string firstName;
        private string lastName;
        private int telephoneNumber;

        public Contact(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public Contact(string firstName, string lastName, int telephoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            TelephoneNumber = telephoneNumber;
        }

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public int TelephoneNumber { get => telephoneNumber; set => telephoneNumber = value; }

        public override string ToString()
        {
            string result = String.Format("First name: {0}, Last name: {1}, Tlf: {2}",
                this.FirstName, this.LastName, this.TelephoneNumber);
            return result;
        }
    }
}
