﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task16
{
    class ContactsSearcher:IEnumerable
    {
        private List<Contact> contacts;

        public ContactsSearcher(List<Contact> contacts)
        {
            this.contacts = contacts;
        }

        internal List<Contact> Contacts { get => contacts; set => contacts = value; }

        public void Search(string name)
        {
            List<Contact> resultList = Contacts.FindAll(contact =>
            (name == contact.FirstName || name == contact.LastName) ||
            (contact.FirstName.ToLowerInvariant().Contains(name) ||
            contact.LastName.ToLowerInvariant().Contains(name.ToLowerInvariant())));
            foreach (var contact in resultList)
            {
                Console.WriteLine(contact.ToString());
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public ContactsSearcherEnum GetEnumerator()
        {
            return new ContactsSearcherEnum(contacts);
        }
    }
}
