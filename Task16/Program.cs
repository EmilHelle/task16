﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task16
{
    class Program
    {
        static void Main(string[] args)
        {
            //set up contact list
            ContactsSearcher contactsSearcher = new ContactsSearcher(new List<Contact>
            {
                new Contact("Sarah", "Jojo"),
                new Contact("Mark", "Toto"),
                new Contact("Tom", "Lo", 23098910),
                new Contact("Chris", "Hems"),
                new Contact("Tony", "Stark", 45239412)
            });

            //enumerating contactsSearcher
            foreach (Contact contact in contactsSearcher)
            {
                Console.WriteLine(contact.FirstName);
            }

            //linq example
            List<Contact> otherContactsCollection = new List<Contact>() { new Contact("Sarah", "Jojo"),
                new Contact("Mark", "o"),
                new Contact("Philip", "mombo", 23098910),
                new Contact("Liam", "Guy"),
                new Contact("Joe", "Jones", 45239412) };

            var query =
            from contact in otherContactsCollection
            where contact.TelephoneNumber != 0
            select contact;

            Console.WriteLine("With Tlf number: ");
            foreach (Contact contact in query)
            {
                Console.Write(contact.FirstName + ", ");
            }
        }
    }
}
