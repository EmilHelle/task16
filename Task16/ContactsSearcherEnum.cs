﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task16
{
    class ContactsSearcherEnum : IEnumerator
    {
        public List<Contact> contacts;
        int position = -1;

        public ContactsSearcherEnum(List<Contact> contacts)
        {
            this.contacts = contacts;
        }

        public bool MoveNext()
        {
            position++;
            return (position < contacts.Count);
        }

       public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Contact Current
        {
            get
            {
                try
                {
                    return contacts[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}
